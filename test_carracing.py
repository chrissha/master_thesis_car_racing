import gym
import utils
from torchvision import transforms
from vae_gym_wrapper import VaeWrapper
from TD3 import TD3
from vae import VaeAgent


# Hyperparameters
vae_dataset_size = 300
latent_size = 32

transform = transforms.Compose([
    transforms.ToPILImage(),
    transforms.Resize((64, 64)),
    transforms.ToTensor()
])

print("Loading vae...")
vae = utils.Vae(latent_size)
vae_agent = VaeAgent(vae)
vae_agent.load()

print("Loading model...")
base_env = gym.make('CarRacing-v0', verbose=0)
wrapped_env_fn = lambda: VaeWrapper(base_env, vae, transform=transform)
model = TD3(wrapped_env_fn)
model.load()


# Run CarRacing
env = wrapped_env_fn()

for i in range(2):
    ep_rew = 0
    obs = env.reset()

    while True:
        act = model.predict(obs)
        obs, rew, done, _ = env.step(act)
        ep_rew += rew
        env.render()
        if done: 
            print("Episode reward: {}".format(ep_rew))            
            break