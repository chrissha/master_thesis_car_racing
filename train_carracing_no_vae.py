import os
import gym
import utils
import time
from torchvision import transforms
from vae_gym_wrapper import VaeWrapper
from TD3 import TD3
from vae import VaeAgent


class Actor(BaseVae):
    """The vae used in car-racing"""
    def __init__(self, latent_size):
        super(Vae, self).__init__()

        self.latent_size = latent_size
        self.fc_size = 2 * 2 * 256 # 1024
        
        # Encoder network
        self.en_cnn1 = nn.Conv2d(3, 32, kernel_size=4, stride=2, padding=0)
        self.en_cnn2 = nn.Conv2d(32, 64, kernel_size=4, stride=2, padding=0)
        self.en_cnn3 = nn.Conv2d(64, 128, kernel_size=4, stride=2, padding=0)
        self.en_cnn4 = nn.Conv2d(128, 256, kernel_size=4, stride=2, padding=0)
        self.en_mean = nn.Linear(self.fc_size, latent_size)
        self.en_std = nn.Linear(self.fc_size, latent_size)

        # Decoder network
        self.de_fc = nn.Linear(latent_size, self.fc_size)
        self.de_cnn1 = nn.ConvTranspose2d(1024, 128, kernel_size=5, stride=2, padding=0)
        self.de_cnn2 = nn.ConvTranspose2d(128, 64, kernel_size=5, stride=2, padding=0)
        self.de_cnn3 = nn.ConvTranspose2d(64, 32, kernel_size=6, stride=2, padding=0)
        self.de_cnn4 = nn.ConvTranspose2d(32, 3, kernel_size=6, stride=2, padding=0)


    def encoder(self, x):
        x = torch.relu(self.en_cnn1(x))
        x = torch.relu(self.en_cnn2(x))
        x = torch.relu(self.en_cnn3(x))
        x = torch.relu(self.en_cnn4(x))
        x = torch.flatten(x, start_dim=1)
        mean = self.en_mean(x)
        std = self.en_std(x)
        return mean, std


    def decoder(self, z):
        x = self.de_fc(z)
        x = x.view(-1, self.fc_size, 1, 1)
        x = torch.relu(self.de_cnn1(x))
        x = torch.relu(self.de_cnn2(x))
        x = torch.relu(self.de_cnn3(x))
        x = torch.sigmoid(self.de_cnn4(x))
        return x


print("Loading vae...")
base_env = lambda: gym.make('CarRacing-v0', verbose=0)

print("Training model...")
model = TD3(base_env, Q_lr=1e-4, mu_lr=1e-3, buffer_size=5000, gamma=0.99, tau=0.05)

start = time.time()
model.train(total_timesteps=100000)
end = time.time()

model.save()
model.make_plot()

print("Finished training in {:.2f} seconds".format(end - start))