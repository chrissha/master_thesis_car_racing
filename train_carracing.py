import os
import gym
import utils
import time
import numpy as np
from torchvision import transforms
from vae_gym_wrapper import VaeWrapper
from TD3 import TD3
from vae import VaeAgent


# Hyperparameters
vae_dataset_size = 10000
latent_size = 32
training_timesteps = 100000

transform = transforms.Compose([
    transforms.ToPILImage(),
    transforms.Resize((64, 64)),
    transforms.ToTensor()
])



# TODO prøv med mindre action space, fjern brems
# TODO fjern opencv-python

print("Loading vae...")
base_env = gym.make('CarRacing-v0', verbose=0)
vae = utils.Vae(latent_size)
vae_agent = VaeAgent(vae, batch_size=64, lr=0.001, epochs=10)

dataset = utils.GymDataset(base_env, transform=transform)
if not os.path.isfile('model_data/dataset'):
    print("Dataset not found, collecting dataset")
    dataset.collect()
    dataset.save()
else: 
    print("Dataset found, loading dataset")
    dataset.load()

if not os.path.isfile('model_data/vae'):
    # Train vae using dataset
    print("Vae not found, training vae")
    vae_agent.train(dataset)
    vae_agent.save()
else:
    print("Vae found, loading vae")
    vae_agent.load()

indicies = np.random.randint(0, len(dataset) - 1, 10)
#vae_agent.print_result(dataset, indicies, filename="carracing_results")
vae_agent.plot_loss(to_file=True, filename="carracing_loss")


"""
print("Training model...")
wrapped_env_fn = lambda: VaeWrapper(base_env, vae, transform=transform)
model = TD3(wrapped_env_fn, Q_lr=5e-5, mu_lr=5e-4, buffer_size=5000, gamma=0.99, tau=0.05)

start = time.time()
model.train(total_timesteps=100000)
end = time.time()

model.save()
model.make_plot()

print("Finished training in {:.2f} seconds".format(end - start))
"""