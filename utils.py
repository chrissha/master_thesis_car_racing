import os
import pickle
import numpy as np
import torch
from torch import nn
from torch.utils.data import Dataset
from torchvision import transforms
from vae import BaseVae


# Vae
class Vae(BaseVae):
    """The vae used in car-racing"""
    def __init__(self, latent_size):
        super(Vae, self).__init__()

        self.latent_size = latent_size
        self.fc_size = 2 * 2 * 256 # 1024
        
        # Encoder network
        self.en_cnn1 = nn.Conv2d(3, 32, kernel_size=4, stride=2, padding=0)
        self.en_cnn2 = nn.Conv2d(32, 64, kernel_size=4, stride=2, padding=0)
        self.en_cnn3 = nn.Conv2d(64, 128, kernel_size=4, stride=2, padding=0)
        self.en_cnn4 = nn.Conv2d(128, 256, kernel_size=4, stride=2, padding=0)
        self.en_mean = nn.Linear(self.fc_size, latent_size)
        self.en_std = nn.Linear(self.fc_size, latent_size)

        # Decoder network
        self.de_fc = nn.Linear(latent_size, self.fc_size)
        self.de_cnn1 = nn.ConvTranspose2d(1024, 128, kernel_size=5, stride=2, padding=0)
        self.de_cnn2 = nn.ConvTranspose2d(128, 64, kernel_size=5, stride=2, padding=0)
        self.de_cnn3 = nn.ConvTranspose2d(64, 32, kernel_size=6, stride=2, padding=0)
        self.de_cnn4 = nn.ConvTranspose2d(32, 3, kernel_size=6, stride=2, padding=0)


    def encoder(self, x):
        x = torch.relu(self.en_cnn1(x))
        x = torch.relu(self.en_cnn2(x))
        x = torch.relu(self.en_cnn3(x))
        x = torch.relu(self.en_cnn4(x))
        x = torch.flatten(x, start_dim=1)
        mean = self.en_mean(x)
        std = self.en_std(x)
        return mean, std


    def decoder(self, z):
        x = self.de_fc(z)
        x = x.view(-1, self.fc_size, 1, 1)
        x = torch.relu(self.de_cnn1(x))
        x = torch.relu(self.de_cnn2(x))
        x = torch.relu(self.de_cnn3(x))
        x = torch.sigmoid(self.de_cnn4(x))
        return x


class GymDataset(Dataset):
    """Class to create a dataset from a gym environment"""
    def __init__(self, env, transform=transforms.ToTensor()):
        """Initializes the dataset
        
        Initializes the dataset by acting randomly in the given environment until the desired size
        of the dataset has been reached.
        
        params:
            env (gym.Env): The gym environment to collect dataset from
            size (int): The size of the dataset
        """
        self.env = env
        self.transform = transform
        self.dataset = []
        

    def __len__(self):
        """Returns the length of the dataset"""
        return len(self.dataset)


    def __getitem__(self, index):
        """Returns the data at the given index"""
        return self.transform(self.dataset[index])

    
    def collect(self, size=10000):
        """Runs the environment and appends the given size to the dataset"""
        dataset = []
        obs = self.env.reset()
        for _ in range(size):
            act = self.env.action_space.sample()
            obs, _, done, _ = self.env.step(act)
            dataset.append(obs)
            
            if done:
                obs = self.env.reset()
                continue

        self.env.close()
        self.dataset = np.array(dataset)


    def save(self, filepath='model_data/dataset'):
        """Saves the dataset to file"""
        if len(self) == 0:
            print("No data in this dataset, cannot save.")
            return
        
        with open(filepath, 'wb') as f:
            pickle.dump(self.dataset, f)


    def load(self, filepath='model_data/dataset'):
        """Load a dataset from file"""
        if not os.path.isfile(filepath):
            print("File not found")
            return

        with open(filepath, 'rb') as f:
            self.dataset = pickle.load(f)