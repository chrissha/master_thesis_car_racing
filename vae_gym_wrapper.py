"""Gym wrapper for adding a variational autoencoder to environment observations"""
import gym
import torch
import numpy as np
from gym.spaces import Box
from torchvision import transforms


class VaeWrapper(gym.ObservationWrapper):
    """Wraps a gym enviornment and overrides the observation to use a vae"""
    def __init__(self, env, vae, transform=None):
        super(VaeWrapper, self).__init__(env)

        self.vae = vae
        self.observation_space = Box(0, 1, (vae.latent_size,))
        self.device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

        if transform is not None:
            self.transform = transform
        else:
            self.transform = transforms.Compose([
                transforms.ToPILImage(),
                transforms.ToTensor()
            ])


    @classmethod
    def from_wrapper(cls, env):
        """Creates a new wrapped environment with the same vae as env"""
        vae = env.vae
        transform = env.transform
        env = gym.make(env.spec.id)
        return cls(env, vae, transform=transform)


    def observation(self, obs):
        """Overrides the observation in this environment"""
        obs = self.transform(obs).unsqueeze(0).to(self.device)
        obs = self.vae.get_latent_vector(obs).cpu().detach().numpy().squeeze()
        return obs
