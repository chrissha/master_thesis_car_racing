import os
import gym
import utils
import torch
import matplotlib.pyplot as plt
from torchvision import transforms
from vae import VaeAgent

latent_size = 32
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

transform = transforms.Compose([
    transforms.ToPILImage(),
    transforms.Resize((64, 64)),
    transforms.ToTensor()
])

def show_im(index):
    # Original image
    im = dataset[index]
    im1 = im.permute(1, 2, 0)
    plt.subplot(1, 2, 1)
    plt.imshow(im1)

    # Reconstructed image
    im2 = im.clone().to(device)
    im2 = im2.unsqueeze(0)
    im2 = vae.encode_decode(im2)
    im2 = im2.squeeze()
    im2 = im2.permute(1, 2, 0).cpu().detach()
    plt.subplot(1, 2, 2)
    plt.imshow(im2)

    plt.show()


# Initialize 
base_env = gym.make('CarRacing-v0', verbose=0)
dataset = utils.GymDataset(base_env, transform=transform)
vae = utils.Vae(latent_size)
vae_agent = VaeAgent(vae, epochs=10, batch_size=64, lr=0.001)

# Load or create dataset
if not os.path.isfile('model_data/dataset'):
    dataset.collect()
    dataset.save()
else: 
    dataset.load()

# Train VAE
#vae_agent.train(dataset)
#vae_agent.save()
vae_agent.load()

# Display results
show_im(5500)
